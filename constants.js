
exports.gmailRegExp = /^[\w-\.]+@gmail+\.+[\w-]{2,4}$/; 
exports.phoneRegExp = /^\+380[0-9]{9}$/; 
exports.passwordRegExp = /(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?!.*?[;\.\s]).{6,}/;
exports.requiredPropsForUser = ['email', 'firstName', 'lastName', 'phoneNumber', 'password'];
exports.requiredPropsForFighter = ["name", "power", "defense", "health"];
