const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { excludePasswordMiddleware } = require('../middlewares/exclude.password.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.post('', createUserValid, (req, res, next) => {     
   try {    
     if (!res.err.error) {           
      const user = UserService.createNewUser(req.body);
      res.data = user;
     }    
   }catch(err) {
     err.status = 404;
     err.error = true; 
     err.message = 'User entity to create is not valid';
     res.err = err;    
   } finally {
     next();
   }  
}, responseMiddleware)

router.get('', (req, res, next) => {
    try {
        const users = UserService.getAllUsers();
        res.data = users;
      }catch(err) {
        err.status = 404;
        err.error = true;
        err.message = 'Users were not found';
        res.err = err;
      } finally {
        next();
      }  
}, responseMiddleware)

router.get('/:id', (req, res, next) => {    
    try {
        const user = UserService.getUserById(req.params.id);
        res.data = user;
      }catch(err) {
        err.status = 404;
        err.error = true;
        err.message = 'User was not found';
        res.err = err;        
      } finally {
        next();
      }  
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {    
    try {
        const user = UserService.removeUser(req.params.id);
        res.data = {message: 'User was removed'};
      }catch(err) {
        err.status = 400;
        err.error = true;
        err.message = 'Failed to remove user';
        res.err = err;        
      } finally {
        next();
      }  
}, responseMiddleware)

router.put('/:id',updateUserValid, (req, res, next) => {     
    try {
      if (!res.err.error) {
        const user = UserService.updateUser(req.params.id, req.body);
        res.data = user;
      }
      }catch(err) {
        err.status = 400;
        err.error = true;
        err.message = 'Failed to update user';
        res.err = err;        
      } finally {
        next();
      }  
}, responseMiddleware)

module.exports = router;