const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { authMiddleware } = require('../middlewares/auth.middleware')

const router = Router();

router.post('/login', authMiddleware, (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        if (!res.err.error) {
            const data = AuthService.login(req.body);
            res.data = data;
        }
    } catch (err) {
        err.status = 404;
        err.message = 'User not found';
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;