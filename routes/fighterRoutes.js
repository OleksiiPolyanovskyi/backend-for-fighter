const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('', (req, res, next) => {
    try {
        const fighters = FighterService.getAllFighters();
        res.data = fighters
      }catch(err) {
        err.status = 404;
        err.error = true
        err.message = 'Fighters were not found';
        res.err = err;
      } finally {
        next();
      }  
}, responseMiddleware)

router.post('',createFighterValid, (req, res, next) => {
    try {
      if (!res.err.error) {
        const fighter = FighterService.createFighter(req.body);
        res.data = fighter;
      }
      }catch(err) {
        err.status = 400;
        err.error = true;
        err.message = 'Fighter entity to create is not valid';
        res.err = err;
      } finally {
        next();
      }  
}, responseMiddleware)

router.get('/:id', (req, res, next) => {    
    try {
        const fighter = FighterService.getFighterById(req.params.id);
        res.data = fighter;
      }catch(err) {
        err.status = 404;
        err.error = true;
        err.message = 'Fighter was not found';
        res.err = err;
      } finally {
        next();
      }  
}, responseMiddleware)

router.delete('/:id', (req, res, next) => {    
    try {
        const fighter = FighterService.removeFighter(req.params.id);
        res.data = {message: 'Fighter was removed'};
      }catch(err) {
        err.status = 404;
        err.error = true;
        err.message = 'Failed to remove fighter';
        res.err = err;        
      } finally {
        next();
      }  
}, responseMiddleware)

router.put('/:id',updateFighterValid, (req, res, next) => {     
    try {
      if (!res.err.error) {
        const fighter = FighterService.updateFighter(req.params.id, req.body);
        res.data = fighter;
      }
      }catch(err) {
        err.status = 400;
        err.error = true;
        err.message = 'Failed to update fighter';
        res.err = err;        
      } finally {
        next();
      }  
}, responseMiddleware)

module.exports = router;