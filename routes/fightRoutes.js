const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router.post('', (req, res, next) => {
    try {             
        const fight = FightService.createFight(req.body);
        res.data = fight;      
      }catch(err) {
        err.status = 400;
        err.error = true;
        err.message = 'Fight entity to create is not valid';
        res.err = err;
      } finally {
        next();
      }  
}, responseMiddleware)

router.get('', (req, res, next) => {
  try {
      const fights = FightService.getAllFights();
      res.data = fights;
    }catch(err) {
      err.status = 404;
      err.error = true
      err.message = 'Fights were not found';
      res.err = err;
    } finally {
      next();
    }  
}, responseMiddleware)

router.delete('/:id', (req, res, next) => { 
  console.log(req.params.id);   
  try {
      const fight = FightService.removeFight(req.params.id);
      res.data = {message: 'Fight was removed'};
    }catch(err) {
      err.status = 404;
      err.error = true;
      err.message = 'Failed to remove fight';
      res.err = err;        
    } finally {
      next();
    }  
}, responseMiddleware)

module.exports = router;