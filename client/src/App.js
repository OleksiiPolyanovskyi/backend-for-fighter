import React from 'react';
import StartScreen from './components/startScreen';
import Arena from './components/arena';
import FightsList from './components/fights-list'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Switch, Route } from 'react-router-dom';
import './App.css';

function App() {

  return (
    <MuiThemeProvider>
      <Switch>
        <Route exact path = '/' component = {StartScreen} />
        <Route path = '/arena' component = {Arena} />
        <Route path = '/fights' component = {FightsList} />
      </Switch>     
    </MuiThemeProvider>
  );
}

export default App;
