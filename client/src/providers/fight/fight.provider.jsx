import React, {createContext, useState } from 'react'

export const FightContext = createContext({
    firstFighter:null,
    secondFighter:null,
    firstIndicator:null,
    secondIndicator:null,
    battleLog:null,    
    setFirstFighter:()=>{},
    setSecondFighter:()=>{},
    setFirstIndicator:()=>{},
    setSecondIndicator:()=>{},
    setBattleLog:()=>{},    
})

const FightProvider = ({children}) =>{
    const [firstFighter, setFirst] = useState();
    const [secondFighter, setSecond] = useState();
    const [firstIndicator, setFirstInd] = useState();
    const [secondIndicator, setSecondInd] = useState();
    const [battleLog, setLog] = useState();    
    const setFirstFighter = fighter => setFirst(fighter);
    const setSecondFighter = fighter => setSecond(fighter);
    const setFirstIndicator = ind => setFirstInd(ind);
    const setSecondIndicator = ind => setSecondInd(ind);
    const setBattleLog = log => setLog(log);

    return(
        <FightContext.Provider value={{
            firstFighter,
            secondFighter,
            firstIndicator,
            secondIndicator,
            battleLog,    
            setFirstFighter,
            setSecondFighter,
            setFirstIndicator,
            setSecondIndicator,
            setBattleLog  
        }}>            
            {children}
        </FightContext.Provider>
    )
}

export default FightProvider