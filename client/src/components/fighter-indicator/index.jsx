import React, {useRef, useContext} from 'react';
import { FightContext } from '../../providers/fight/fight.provider'

import './indicator.css'

function FighterIndicator ({fighter, token}) {    
    const indicator = useRef();
    const context = useContext(FightContext)
    token === 'first' ? context.setFirstIndicator(indicator)
                      : context.setSecondIndicator(indicator)
    return (            
        <div className="arena___fighter-indicator">
            <span className="arena___fighter-name">{fighter}</span>
            <div className="arena___health-indicator">
                <div className="arena___health-bar" ref={indicator}></div>
            </div>
        </div>                    
    );   
}

export default FighterIndicator;