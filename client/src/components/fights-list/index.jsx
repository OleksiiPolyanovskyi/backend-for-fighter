import React, {useState, useEffect}  from 'react';
import { getFights, deleteFight } from '../../services/domainRequest/fightRequest';
import FightItem from '../fight-item'


import './fights.css'

function FightsList () {
    const [fights, setFights] = useState([]);

    useEffect(() => {
        getFights()        
        .then( fights => {
            if(fights && !fights.error) {
                setFights(fights);                
            }
        })        
    }, []);

    const removeFight = (id) => {        
        deleteFight(id)
        .then( fight => {
            if(fight && !fight.error) {
                let newfights = fights.filter(fight => fight.id !== id) 
                setFights(newfights)              
            }
        })     
    }
      
    return (
       <div className="fights__container">
           <h2>Saved battles</h2>
           <ul className="fights__list">
               {fights.map(fight => {
                  return <FightItem key={fight?.id} fight = {fight} removeFight = {removeFight}/>
               })}
           </ul>
       </div>       
    );   
}

export default FightsList;