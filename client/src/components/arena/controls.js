export const controls = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCritical: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCritical: ['KeyU', 'KeyI', 'KeyO']
}