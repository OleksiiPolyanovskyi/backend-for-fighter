import React, {useState ,useContext, useEffect} from 'react';
import FighterIndicator from '../fighter-indicator';
import Modal from '../modal'
import { FightContext } from '../../providers/fight/fight.provider';
import { fight } from './fight';
import { createFight } from '../../services/domainRequest/fightRequest';

import './arena.css'

function Arena () { 
    const [winner, setWinner] = useState(null);      
    const context = useContext(FightContext);
    useEffect(()=>{
        if (context.firstIndicator && context.secondIndicator){
            fight(context.firstFighter, context.secondFighter, context.firstIndicator.current, context.secondIndicator.current)
            .then( res => {
                setWinner(res.winner);
                context.setBattleLog(res.log);                
            })
        }        
    },[context.firstIndicator, context.secondIndicator]); 

    const saveBattle = async (context) => {
        const fight = {
            fighter1: context.firstFighter.id,
            fighter2: context.secondFighter.id,
            log: context.battleLog
        }
        const newfight = await createFight(fight)       
    }
    
    return (
        <div className="arena___root">
            <div className="arena___fight-status">
                <FighterIndicator fighter={context.firstFighter?.name} token="first"/>
                <div className="arena___versus-sign"></div>
                <FighterIndicator fighter={context.secondFighter?.name} token="second"/>
            </div>
            <div className="arena___battlefield">
                <div className="arena___fighter arena___left-fighter">
                    <img className="fighter-preview___img" src="https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif" title="Ryu" alt="Ryu"/>
                </div>
                <div className="arena___fighter arena___right-fighter">
                    <img className="fighter-preview___img" src="https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif" title="Ken" alt="Ken"/>
                </div>
            </div>
            {winner ? <Modal winner={winner} saveBattle={saveBattle}/> : null}
        </div>            
    );   
}

export default Arena;