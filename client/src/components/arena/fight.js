import { controls } from './controls';

export async function fight(firstFighter, secondFighter, leftFighterIndicator, rightFighterIndicator) {

  const state = {
    first: {
      fighterInDefence: false,
      fighterHealth: firstFighter.health,
      fighterCritical: new Set(),
      fighterCriticalAllowed: true,
      fighterIndicatorWidth: 100,
      hits: 0
    },
    second: {
      fighterInDefence: false,
      fighterHealth: secondFighter.health,
      fighterCritical: new Set(),
      fighterCriticalAllowed: true,
      fighterIndicatorWidth: 100,
      hits: 0
    }    
  };  

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over    
    document.addEventListener('keydown', event => {
     
      switch(event.code){
        case controls.PlayerOneAttack:                  
          changeStateForHit(state, firstFighter, secondFighter, rightFighterIndicator, 'first', 'second');          
          state.first.hits += 1;
          if (state.second.fighterHealth < 0) resolve({winner:firstFighter, log: createBattleLog(state)});
          break;
        case controls.PlayerTwoAttack:
          changeStateForHit(state, firstFighter, secondFighter, leftFighterIndicator, 'second', 'first');      
          state.second.hits += 1;
          if (state.first.fighterHealth < 0) resolve({winner:secondFighter, log: createBattleLog(state)});
          break;
        case controls.PlayerOneBlock:
          state.first.fighterInDefence = true;             
          break;
        case controls.PlayerTwoBlock:
          state.second.fighterInDefence = true;         
          break;
        case controls.PlayerOneCritical[0]:
          state.first.fighterCritical.add(controls.PlayerOneCritical[0]);         
          if (state.first.fighterCriticalAllowed){
            changeStateForCriticalHit(firstFighter, secondFighter, state, 'first', 'second', rightFighterIndicator)
          }  
          if (state.second.fighterHealth < 0) resolve({winner:firstFighter, log: createBattleLog(state)});
          break;
        case controls.PlayerOneCritical[1]:
          state.first.fighterCritical.add(controls.PlayerOneCritical[1]);          
          if (state.first.fighterCriticalAllowed){
            changeStateForCriticalHit(firstFighter, secondFighter, state, 'first', 'second', leftFighterIndicator)
          }  
          if (state.second.fighterHealth < 0) resolve({winner:firstFighter, log: createBattleLog(state)});
          break;
        case controls.PlayerOneCritical[2]:
          state.first.fighterCritical.add(controls.PlayerOneCritical[2]);
          if (state.first.fighterCriticalAllowed){
            changeStateForCriticalHit(firstFighter, secondFighter, state, 'first', 'second', rightFighterIndicator)
          }              
          if (state.second.fighterHealth < 0) resolve({winner:firstFighter, log: createBattleLog(state)});
          break;
        case controls.PlayerTwoCritical[0]:
          state.second.fighterCritical.add(controls.PlayerTwoCritical[0]);
          if (state.second.fighterCriticalAllowed){
            changeStateForCriticalHit(secondFighter, firstFighter, state, 'second', 'first', leftFighterIndicator)
          } 
          if (state.first.fighterHealth < 0) resolve({winner:secondFighter, log: createBattleLog(state)});  
          break;
        case controls.PlayerTwoCritical[1]:
          state.second.fighterCritical.add(controls.PlayerTwoCritical[1]);
          if (state.second.fighterCriticalAllowed){
            changeStateForCriticalHit(secondFighter, firstFighter, state, 'second', 'first', leftFighterIndicator)
          } 
          if (state.first.fighterHealth < 0) resolve({winner:secondFighter, log: createBattleLog(state)});   
          break;
        case controls.PlayerTwoCritical[2]:
          state.second.fighterCritical.add(controls.PlayerTwoCritical[2]);
          if (state.second.fighterCriticalAllowed){
            changeStateForCriticalHit(secondFighter, firstFighter, state, 'second', 'first', leftFighterIndicator)
          } 
          if (state.first.fighterHealth < 0) resolve({winner:secondFighter, log: createBattleLog(state)});   
          break;
      }
    })
    document.addEventListener('keyup', event => {       
      switch(event.code){        
        case controls.PlayerOneBlock:
          state.first.fighterInDefence = false;         
          break;
        case controls.PlayerTwoBlock:
          state.second.fighterInDefence = false;         
          break;
        case controls.PlayerOneCritical[0]:
          state.first.fighterCritical.delete(controls.PlayerOneCritical[0]);          
          break;
        case controls.PlayerOneCritical[1]:
          state.first.fighterCritical.delete(controls.PlayerOneCritical[1]);         
          break;
        case controls.PlayerOneCritical[2]:
          state.first.fighterCritical.delete(controls.PlayerOneCritical[2]);          
          break;
        case controls.PlayerTwoCritical[0]:
          state.second.fighterCritical.delete(controls.PlayerTwoCritical[0]);            
          break;
        case controls.PlayerTwoCritical[1]:
          state.second.fighterCritical.delete(controls.PlayerTwoCritical[1]);            
          break;
        case controls.PlayerTwoCritical[2]:
           state.second.fighterCritical.delete(controls.PlayerTwoCritical[2]);            
          break;
      }
    })
  });
}

export function getDamage(attacker, defender, fighterInDefence) {
  // return damage  
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);  
  if (hitPower > blockPower && !fighterInDefence){
    return hitPower - blockPower;
  }else{
    return 0;
  }
}

export function getHitPower(fighter) {
  // return hit power
  if (fighter){
    const criticalHitChance = Math.random() + 1;   
    const power = fighter.power * criticalHitChance;
    return power;
  }else {
    return 0
  }
}

export function getBlockPower(fighter) {
  // return block power 
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;  
} 

function changeStateForHit(state, firstFighter, secondFighter, indicator, attacker, defender){
    const damage = getDamage(state[attacker].fighterInDefence? null : firstFighter, secondFighter, state[defender].fighterInDefence);
    state[defender].fighterHealth = state[defender].fighterHealth - damage;
          
    if (damage > 0) {
        state[defender].fighterIndicatorWidth = state[defender].fighterIndicatorWidth - (damage * 100)/secondFighter.health;
        indicator.style.width =  `${state[defender].fighterIndicatorWidth}%`;
    }  
}

 function changeStateForCriticalHit(fighterAttacker, fighterDefender, state, attacker, defender, indicator) {
    let damage = getCriticalDamage(fighterAttacker, state[attacker].fighterCritical)
    if (damage > 0) {
      state[attacker].hits += 1;
      state[defender].fighterHealth = state[defender].fighterHealth - damage;
      state[attacker].fighterCriticalAllowed = false;
      state[defender].fighterIndicatorWidth = state[defender].fighterIndicatorWidth - (damage * 100)/fighterDefender.health
      indicator.style.width =  `${state[defender].fighterIndicatorWidth}%`
      setTimeout(()=>{
        state[attacker].fighterCriticalAllowed = true;
        console.log('combination allowed');
      },10000)
    }             
  }

  function getCriticalDamage(fighter, combination){
    if (combination.size === 3) {
      return fighter.power * 2;
    }else {
      return 0;
    }  
  }

  function createBattleLog(state) {
    const fighter1Shot = state.first.hits;
    const fighter2Shot = state.second.hits;
    let fighter1Health = Math.floor(state.first.fighterHealth);
    let fighter2Health = Math.floor(state.second.fighterHealth);
    fighter1Health = fighter1Health < 0 ? 0 : fighter1Health;
    fighter2Health = fighter2Health < 0 ? 0 : fighter2Health;
    return [{
      fighter1Shot,
      fighter2Shot,
      fighter1Health,
      fighter2Health
    }]
  }