import React, {useContext}  from 'react';
import { Button } from '@material-ui/core';
import { FightContext } from '../../providers/fight/fight.provider';
import { Link } from 'react-router-dom';

import './modal.css'

function Modal ({winner, saveBattle}) { 
    const context = useContext(FightContext);
    const saveHandler = () => {
        saveBattle(context)
    }   
    return (
       <div className="modal__container">
           <Link to="/">
                <span className="modal__close">&times;</span>
           </Link>           
            <h3>Winner: {winner.name}</h3>
            <div className="modal__button-wrapper">
                <Link className="modal__link" to="/">
                    <Button onClick={saveHandler} variant="contained" color="primary" size="small">
                        Save battle
                    </Button>
                </Link>               
            </div>
       </div>       
    );   
}

export default Modal;