import React, {useState, useEffect, useContext} from 'react';
import { getFighters } from '../../services/domainRequest/fightersRequest';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';
import { FightContext } from '../../providers/fight/fight.provider';
import { Link } from 'react-router-dom';

import './fight.css'

function Fight({history}) {
    const [fighters, setFighters] = useState([]);
    const [fighter1, setFighter1] = useState(null);
    const [fighter2, setFighter2] = useState(null);
    const context = useContext(FightContext);
        
    useEffect(() => {
        getFighters()        
        .then( fighters => {
            if(fighters && !fighters.error) {
                setFighters(fighters);
            }
        })        
    }, [])

    const onFightStart = () => {
        if (fighter1 && fighter2) {
            history.push('/arena')
        }      
    }

    const onCreate = (fighter) => {
        setFighters([...fighters, fighter]);
    }

    const onFighter1Select = (fighter1) => {
        setFighter1(fighter1);
        context.setFirstFighter(fighter1);        
    }

    const onFighter2Select = (fighter2) => {
        setFighter2(fighter2);
        context.setSecondFighter(fighter2);
    }

    const getFighter1List = () => {        
        if(!fighter2) {
            return fighters;
        }
        return fighters.filter(it => it.id !== fighter2.id);
    }

    const getFighter2List = () => {        
        if(!fighter1) {
            return fighters;
        }
        return fighters.filter(it => it.id !== fighter1.id);
    }

        return (
            <div id="wrapper">
                <NewFighter onCreated={onCreate} />
                <div id="figh-wrapper">
                    <Fighter selectedFighter={fighter1} onFighterSelect={onFighter1Select} fightersList={getFighter1List() || []} />
                    <div className="btn-wrapper">
                        <Button onClick={onFightStart} variant="contained" color="primary">Start Fight</Button>
                    </div>
                    <Fighter selectedFighter={fighter2} onFighterSelect={onFighter2Select} fightersList={getFighter2List() || []} />
                </div>
                <Link className="fights-link" to="/fights">SAVED BATTLES</Link>
            </div>
        );    
}

export default Fight;