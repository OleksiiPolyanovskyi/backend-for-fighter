import React from 'react';
import { Button } from '@material-ui/core';

import './fight-item.css';

function FightItem({fight, removeFight}) {

    const removeFightHandler = () => {
        removeFight(fight.id)
    }
        
    return (
       <li className="fight__container">           
           <div className="fight__fighter">
              <h3>First fighter: {fight?.fighter1.name}</h3>
              <h3>Hits: {fight?.log[0].fighter1Shot}</h3>
              <h3>Health: {fight?.log[0].fighter1Health}</h3>
           </div>
           <div className="fight__fighter">
              <h3>Second fighter: {fight?.fighter2.name}</h3>
              <h3>Hits: {fight?.log[0].fighter2Shot}</h3>
              <h3>Health: {fight?.log[0].fighter2Health}</h3>
           </div> 
            <Button variant="contained" color="secondary" onClick={removeFightHandler}>Delete</Button>        
       </li>       
    );   
}

export default FightItem;