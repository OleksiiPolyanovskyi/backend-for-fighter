const excludePasswordMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
     if (res.data && res.data.length) {
        const users = res.data.map(user => {
            const {password, ...otherProps} = user;
            return otherProps;
        })
        res.data = users
     }
     else if (res.data) {
         const {password, ...otherProps} = res.data;
         res.data = otherProps;
     }    
     next();
 }
 
 exports.excludePasswordMiddleware = excludePasswordMiddleware;