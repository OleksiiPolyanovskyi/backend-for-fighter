const { fighter } = require('../models/fighter');
const { FighterRepository } = require('../repositories/fighterRepository');
const {requiredPropsForFighter} = require('../constants');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const error = {
        error: false,
        message: ''
    }    
    const superfluous = Object.keys(req.body).filter(el => !requiredPropsForFighter.includes(el))
    if (superfluous.length > 0){
        error.error = true;
        error.message = `Provide only allowed fields`;
    }

    if(req.body.id){
        error.error = true;
        error.message = `Id should not be in req.body`;  
    } 

    if(!req.body.name || !req.body.power) {
        error.error = true;
        error.message = `Please fill all required fields`;        
    }
    
    if (!req.body.health) req.body.health = fighter.health
    if (!req.body.defense) req.body.defense = fighter.defense

    if(typeof req.body.power !== 'number' || typeof req.body.defense !== 'number'){
        error.error = true;
        error.message = `Fighter characteristics has to be numbers`;  
    }
    
    if(req.body.power < 0 || req.body.power > 100) {
        error.error = true;
        error.message = `Power has to be between 0 and 100`;        
    }

    if(req.body.defense < 0 || req.body.defense > 10) {
        error.error = true;
        error.message = `Defense has to be between 0 and 10`;        
    }
    
    const name = req.body.name
    const boxer = FighterRepository.getOne({name})
    if (boxer) {
        error.error = true;
        error.message = `This name is already taken`;
    }
    res.err = error
    
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const error = {
        error: false,
        message: ''
    }
    //Allowed to update only name, power, defence, health.    
    const superfluous = Object.keys(req.body).filter(el => !requiredPropsForFighter.includes(el))
    if (superfluous.length > 0){
        error.error = true;
        error.message = `Provide only allowed fields`;
    }

    if(req.body.id){
        error.error = true;
        error.message = `Id should not be in req.body`;  
    } 

    const id = req.params.id
    const boxer = FighterRepository.getOne({id})

    if (!req.body.name) req.body.name = boxer.name
    if (!req.body.power) req.body.power = boxer.power
    if (!req.body.health) req.body.health = boxer.health
    if (!req.body.defense) req.body.defense = boxer.defense

    if(typeof req.body.power !== 'number' || typeof req.body.defense !== 'number'){
        error.error = true;
        error.message = `Fighter characteristics has to be numbers`;  
    }
    
    if(req.body.power < 0 || req.body.power > 100) {
        error.error = true;
        error.message = `Power has to be between 0 and 100`;        
    }

    if(req.body.defense < 0 || req.body.defense > 10) {
        error.error = true;
        error.message = `Defense has to be between 0 and 10`;        
    }
    res.err = error
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;