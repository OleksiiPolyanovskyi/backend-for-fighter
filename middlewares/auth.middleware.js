const authMiddleware = (req, res, next) => {
    const error = {
        error: false,
        message: ''
    }
    if (!req.body.email || ! req.body.password) {
        error.error = true;
        error.message = `Please provide all credentials`
    }
    res.err = error;
    next();
 }
 
 exports.authMiddleware = authMiddleware;