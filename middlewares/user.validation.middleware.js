const { user } = require('../models/user');
const {validateEmail, validatePhoneNumber, validatePassword} = require('./validation.helpers')
const { UserRepository } = require('../repositories/userRepository');
const {requiredPropsForUser} = require('../constants')

const createUserValid = (req, res, next) => {

    // TODO: Implement validatior for user entity during creation
    const error = {
        error: false,
        message: ''
    }
    //All required props     
    const propsFromRequest = Object.keys(req.body);
    requiredPropsForUser.forEach(prop => {
        if (!propsFromRequest.includes(prop)){
            error.error = true;
            error.message = `Please fill ${prop} field`;           
        } 
    })
    const superfluous = Object.keys(req.body).filter(el => !requiredPropsForUser.includes(el))
    if (superfluous.length > 0){
        error.error = true;
        error.message = `Provide only allowed fields`;
    }
   
    //Valid email
    if (req.body.email && validateEmail(req.body.email) === -1) {
        error.error = true;
        error.message = `This service is only for gmail users`;         
    }
    //Valid phone number
    if (req.body.phoneNumber && validatePhoneNumber(req.body.phoneNumber) === -1) {
        error.error = true;
        error.message = `Please provide phone number in a format of +380xxxxxxxxx`;        
    }
    //Password complixity
    if (req.body.password && req.body.password.length < 3) {
        error.error = true;
        error.message = `Password must be at least 3 chars`;  
    }
    //Email is already registered
    const email = req.body.email
    let user = UserRepository.getOne({email})
    if (user) {
        error.error = true;
        error.message = `User with this email is already registered`; 
    }
    //Phone number is already registered
    const phoneNumber = req.body.phoneNumber
    user = UserRepository.getOne({phoneNumber})
    if (user) {
        error.error = true;
        error.message = `User with this phone number is already registered`; 
    }
    
    res.err = error
      
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const error = {
        error: false,
        message: ''
    }
    //Allowed to update only email, firstName, lastName, phoneNumber.    
    const superfluous = Object.keys(req.body).filter(el => !requiredPropsForUser.includes(el))
    if (superfluous.length > 0){
        error.error = true;
        error.message = `Provide only allowed fields`;
    }
    //Valid email
    if (req.body.email && validateEmail(req.body.email) === -1) {
        error.error = true;
        error.message = `This service is only for gmail users`;         
    }
    //Valid phone number
    if (req.body.phoneNumber && validatePhoneNumber(req.body.phoneNumber) === -1) {
        error.error = true;
        error.message = `Please provide phone number in a format of +380xxxxxxxxx`;        
    }
    res.err = error
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;