const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (res.err && res.err.error) {
        res.status(res.err.status || 400).send(res.err)
    }
    res.status(200).send(res.data)
    next();
}

exports.responseMiddleware = responseMiddleware;