const { gmailRegExp, phoneRegExp, passwordRegExp } = require('../constants')

function validateEmail(email){
   return email.search(gmailRegExp);     
}

function validatePhoneNumber(number){
    return number.search(phoneRegExp);
}
function validatePassword(password){
    return password.search(passwordRegExp);
}

module.exports = {validateEmail, validatePhoneNumber, validatePassword};
