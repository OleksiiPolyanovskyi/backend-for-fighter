const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    createNewUser(user) {        
        const newUser = UserRepository.create(user)
        if(!newUser) {
            return null;
        } 
        return newUser;
    }
    getAllUsers() {
        const users = UserRepository.getAll();
        if(!users) {
            return null;
        }
        return users;
    }

    getUserById(id) {
        const user = UserRepository.getOne({id});
        if(!user) {            
            throw new Error();
        }
        return user;
    }

    removeUser(id) {
        const user = UserRepository.delete(id);
        if(user.length === 0) {            
            throw new Error();
        }
        return user;
    }

    updateUser(id, params) {
        const user = UserRepository.update(id, params);
        if(!user.id) {            
            throw new Error();
        }
        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();