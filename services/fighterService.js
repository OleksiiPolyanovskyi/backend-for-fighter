const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAllFighters() {
        const fighters = FighterRepository.getAll();
        if(!fighters) {
            return null;
        }
        return fighters;
    }
    createFighter(fighter) {
        const newFighter = FighterRepository.create(fighter)
        if(!newFighter) {
            return null;
        } 
        return newFighter;
    }
    getFighterById(id) {
        const fighter = FighterRepository.getOne({id});
        if(!fighter) {
            throw new Error();
        }
        return fighter;
    }
    removeFighter(id) {
        const fighter = FighterRepository.delete(id);
        if(fighter.length === 0) {            
            throw new Error();
        }
        return fighter;
    }

    updateFighter(id, params) {
        const fighter = FighterRepository.update(id, params);
        if(!fighter.id) {            
            throw new Error();
        }
        return fighter;
    }
}

module.exports = new FighterService();