const { FightRepository } = require('../repositories/fightRepository');
const { FighterRepository } = require('../repositories/fighterRepository');

class FightService {
    // OPTIONAL TODO: Implement methods to work with fights
    createFight(fight) {
        const newFight = FightRepository.create(fight)
        if(!newFight) {
            return null;
        } 
        return newFight;
    }

    getAllFights() {
        const fights = FightRepository.getAll();
        if(!fights) {
            return null;
        }
        const populatedFights = fights.map(fight => {  
            const {fighter1, fighter2, ...other} = fight;                          
            return { 
                fighter1: FighterRepository.getOne({id: fighter1}),
                fighter2: FighterRepository.getOne({id: fighter2}),                 
                ...other
            }
        })
        return populatedFights;
    }

    removeFight(id) {
        const fight = FightRepository.delete(id);
        if(fight.length === 0) {            
            throw new Error();
        }
        return fight;
    }
}

module.exports = new FightService();